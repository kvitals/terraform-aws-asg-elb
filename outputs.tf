# Output data
output "vpc_cidr" {
  value = data.aws_vpc.vpc.cidr_block
}

output "elb_dns_name" {
  description = "The DNS name of the ELB"
  value       = aws_elb.classic_elb.dns_name
}