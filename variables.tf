# AWS Region
variable "region" {
  description = "The region where to deploy the code"
  type = string
  default = "eu-central-1"
}
# AWS Credentials
variable "access_key" {
  description = "AWS Access Key"
  default = ""
}
variable "secret_key" {
  description = "AWS Secret Key"
  default = ""
}
# EC2 instance type & image
variable "image_id" {
  description = "The id of the machine image (AMI) to use for the server"
  type = string
  default = "ami-00a205cb8e06c3c4e"
}
variable "instance_type" {
  description = "Type of the instance to run (e.g. t2.micro, m1.xlarge, c1.medium etc)"
  type = string
  default = "t2.micro"
}
# Min, Max & Desired capacity of EC2 instances in ASG
variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG"
  type        = number
  default = 1
}
variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG"
  type        = number
  default = 1
}
variable "desired_capacity" {
  description = "The desired number of EC2 Instances in the ASG"
  type        = number
  default = 1
}