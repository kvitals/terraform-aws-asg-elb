# Access data about VPC, AZ, Subnets
data "aws_vpc" "vpc" {
}
data "aws_availability_zones" "az" {
}
data "aws_subnet_ids" "default_subnet" {
  vpc_id = data.aws_vpc.vpc.id
}
# Create  AWS Security Group
resource "aws_security_group" "ssh_elb" {
  name = "ssh-elb-sec-group"
  vpc_id = data.aws_vpc.vpc.id
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [data.aws_vpc.vpc.cidr_block]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}
# Create launch template
resource "aws_launch_template" "ec2_instance_template" {
  name_prefix = "ec2_instance_template"
  image_id = var.image_id
  instance_type = var.instance_type
  security_group_names = [aws_security_group.ssh_elb.name]
}
#Create AWS ELB
resource "aws_elb" "classic_elb" {
  subnets = data.aws_subnet_ids.default_subnet.ids
  security_groups = [aws_security_group.ssh_elb.id]

  listener {
    instance_port = 22
    instance_protocol = "tcp"
    lb_port = 22
    lb_protocol = "tcp"
  }
}
# Create AWS ASG
resource "aws_autoscaling_group" "asg" {
  availability_zones = data.aws_availability_zones.az.names
  desired_capacity = var.desired_capacity
  max_size = var.max_size
  min_size = var.min_size
  load_balancers = [aws_elb.classic_elb.name]

  launch_template {
    id = aws_launch_template.ec2_instance_template.id
  }
}
