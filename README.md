#### Requarements:

- Terraform version >= 0.13

### Usage
- Downloading the Plugin for the AWS provider:

`terraform init`

- Generates an execution plan for Terraform:

`terraform plan`

- Build the infrastructure according to Terraform configuration files:

`terraform apply`

- to delete Terraform-managed infrastructure - use:

`terraform destroy`